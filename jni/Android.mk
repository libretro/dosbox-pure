#
#  Copyright (C) 2020-2021 Bernhard Schelling
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

# https://stackoverflow.com/a/16309684
define walk
  $(wildcard $(1)) $(foreach e, $(wildcard $(1)/*), $(call walk, $(e)))
endef

LOCAL_PATH := $(call my-dir)
CORE_DIR   := $(LOCAL_PATH)/..

include $(CLEAR_VARS)
LOCAL_MODULE       := retro
LOCAL_SRC_FILES    := $(patsubst $(LOCAL_PATH)/%, %, $(filter %.cpp, $(call walk, ..)))
LOCAL_C_INCLUDES   := $(CORE_DIR)/include
LOCAL_CPPFLAGS     := -std=c++11 $(COMMON_FLAGS)
LOCAL_LDFLAGS      := -Wl,-version-script=$(CORE_DIR)/link.T
LOCAL_LDLIBS       := -llog
LOCAL_CPP_FEATURES := rtti exceptions
LOCAL_ARM_MODE     := arm
include $(BUILD_SHARED_LIBRARY)
